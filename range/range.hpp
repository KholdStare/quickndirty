/*
 * Quick and dirty range implementation, to use
 * in a loop.
 *
 * Author : Alexander Kondratskiy
 */

#ifndef _QND_RANGE_H_
#define _QND_RANGE_H_

#include <iterator>

namespace qnd {

    // Iterator is the state machine that loops through the range
    template<typename T>
    class range_iterator {
    public:
        typedef std::forward_iterator_tag category;
        typedef T                         value_type;
        typedef T*                        pointer;
        typedef T&                        reference;

        inline range_iterator(T i, T inc) :
            _i(i), _inc(inc) { }

        inline range_iterator<T>& operator ++() {
            _i += _inc;
            return *this;
        }

        inline range_iterator<T> operator ++(int) {
            range_iterator<T> temp(*this);
            _i += _inc;
            return temp;
        }

        inline bool operator ==(const range_iterator<T>& other) const {
            return _i == other._i;
        }

        inline bool operator !=(const range_iterator<T>& other) const {
            return !this->operator==(other);
        }

        inline T const& operator *() const {
            return _i;
        }
        
    private:
        T _i;
        T const _inc;
    };


    // Defines some range over integral types.
    // Essentially a builder for the iterators that go through the range
    template<typename T>
    class range {

    public:
        typedef range_iterator<T> const_iterator;
        
        range(T start, T end, T inc = 1) :
            _start(start), _end(end), _inc(inc) { }


        inline const_iterator begin() const {
            return range_iterator<T>(_start, _inc);
        }

        inline const_iterator end() const {
            return range_iterator<T>(_end, _inc);
        }

    private:
        T const _start;
        T const _end;
        T const _inc;
    };


}

#endif // _QND_RANGE_H_
