/* Playground for range tests. Requires C++11
 */

#include "range.hpp"

#include <iostream>
#include <algorithm>

using namespace std;
typedef qnd::range<int> int_range;

int main(int argc, char* argv[]) {

    // using range-for
    for (auto i : int_range(1, 30)) {
        cout << i << endl;
    }

    // using std::foreach and lambdas
    int_range r(1, 30);
    for_each (begin(r), end(r), [](int i){
        cout << i << endl;
    });

    return 0;
}
